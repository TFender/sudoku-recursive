import java.io.*;

/**
 * Created by Willem van der Tuuk on 17-11-2017.
 */

public class Sudoku {

    // variabele van de grid size, je kan ook een andere grootte sudoku invoeren:)
    int gridSize = 9;

    String fileName;
    File file;

    //multidimensionale array van Integers, deze worden een voor een gezet, en als er een oplossing bestaat,
    //een voor een uitgeprint
    int[][] grid = new int[gridSize][gridSize];

    // constructor, instatie van het object, deze krijgt als argument de bestandsnaam mee zonder .txt
    public Sudoku(String fileName) {
        this.fileName = fileName;
        file = new File(fileName + ".txt");
    }

    // laad de nummers in vanuit de txt file. Deze worden 1 voor 1 in de grid[][] multi-array gedrukt.
    public void loadNumbers() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            int y = 0;
            while ((line = br.readLine()) != null) {
                int x = 0;
                for (char s : line.toCharArray()) {
                    grid[y][x] = Integer.parseInt("" + s);
                    x++;
                }
                y++;
            }
        }
    }

    //recursieve code!
    public boolean crackCode(int x, int y) {
        //zijn we op het eind gekomen? dan geven we true terug en breken we uit de methode
        if (y == gridSize) return true;

        //heeft dit al een waarde? dan hoeven we niet na te denken over de mogelijkheden en gaan we door naar het volgende sudoku vakje,
        //met dezelfde methode. Ik check ook of ik aan het einde van de rij ben beland.
        if (grid[y][x] != 0) {
            if (x == gridSize - 1) {
                if (crackCode(0, y + 1)) return true; //* plek X
            } else {
                if (crackCode(x + 1, y)) return true; //* plek X
            }
        } else {
            // als het sudcku vakje een 0 bevat, dan mogen we kijken of we een getal kunnen invullen!
            for (int number = 1; number <= gridSize; number++) {
                //ga langs alle cijfers die je mogelijk zou kunnen invullen (bv eerst 1, dan 2 enz t/m 9)
                if (rowIsPossible(x, y, number) && columnIsPossible(x, y, number) && boxIsPossible(x, y, number)) {
                    //als we voldoen aan de regels, dan zetten we de variabele number op de grid met het x en y coordinaat
                    //daarna gaan we door naar het volgende sudoku vakje, met dezelfde methode.
                    grid[y][x] = number;
                    if (x == gridSize - 1) {
                        if (crackCode(0, y + 1)) return true; //* plek X
                    } else {
                        if (crackCode(x + 1, y)) return true; //* plek X
                    }
                    grid[y][x] = 0;

                }
            }
        }
        //het is niet gelukt om een cijfer in te voeren in dit vakje. dus in de voorgaande vakjes is een fout gemaakt.
        //Ik geef false terug waardoor je op plek X terug komt in de vorige aanroep.
        return false;
    }


    public boolean rowIsPossible(int x, int y, int number) {
        // een regel waaraan moet worden voldaan, mag dit nummber op deze rij staan?
        for (int i = 0; i < gridSize; i++) {
            if (grid[y][i] == number) return false;
        }
        return true;
    }

    public boolean columnIsPossible(int x, int y, int number) {
        // een regel waaraan moet worden voldaan, mag dit nummber in deze kolom staan?
        for (int i = 0; i < gridSize; i++) {
            if (grid[i][x] == number) return false;
        }
        return true;
    }

    public boolean boxIsPossible(int x, int y, int number) {
        int xbound = x / 3 * 3; // kijk in welke 'box' dit valt x coordinaat ondergrens
        int ybound = y / 3 * 3; // kijk in welke 'box' dit valt y coordinaat ondergrens

        for (int i = ybound; i < ybound + 3; i++) {
            for (int j = xbound; j < xbound + 3; j++) {
                if (grid[i][j] == number) return false;
            }
        }
        return true;
    }


    public void writeSolution() throws FileNotFoundException {
        //schrijf de oplossing naar een nieuwe txt file, met wat fancy opmaak
        PrintWriter writer = new PrintWriter(fileName + "-solution.txt");
        for (int y = 0; y < gridSize; y++) {
            if (y % 3 == 0 && y != 0) writer.println("--------- | --------- | ---------");
            for (int x = 0; x < gridSize; x++) {
                if (x % 3 == 0 && x != 0) writer.print(" | ");
                writer.print(" " + grid[y][x] + " ");
            }
            writer.println();
        }
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        //public static void main betekend in Java taal, hier mag ik iets uitvoeren

        //ik maak een nieuw object aan van mijn java klasse. hier kan ik vervolgens mijn methoden op aanroepen.
        Sudoku sudoku = new Sudoku("sudoku");
        sudoku.loadNumbers();
        //als de sukodu valt op lossen print ik de oplossing
        if (sudoku.crackCode(0, 0)) sudoku.writeSolution();
        else System.out.println("VALT NIET OP TE LOSSEN!!!");
    }

}
